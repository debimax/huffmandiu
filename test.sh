#!/bin/bash
# Hautcoeur Stéphane
# Meilland Jean_Claude
# Melioli Edith
# Rajaomanana Ingrid
# programme qui compare les fichiers de départ    candide.txt aaaa.txt   lorem.txt     
# avec les fichiers respectivement encodés qui a été encodé puis décodé
# On fait un diff avec le fichiers décodés


#NomFile="candide.txt"
NomProg="./huffman-prog.py"

for NomFile  in  candide.txt aaaa.txt   lorem.txt
do
    echo "compare le taux de compression avec le  fichier $NomFile"
    
    $NomProg -c $NomFile
    $NomProg -d $NomFile.enc
    
    # Taille  en byte des fichiers
    TailleFile=$( stat -c %s $NomFile )
    TailleFileEnc=$(stat -c %s $NomFile.enc)
    
    # On  divise par 8 (8 bits)
    TailleFileEnc=$( echo "$TailleFileEnc/8" |bc -l )
    # On  calcule  le taux de compression
    Taux_compression=$( echo "$TailleFileEnc/$TailleFile"  |  bc -l)
    
    echo "Taux de compression $Taux_compression"
        
    #compare le fichier de départ avec le fichier qui a été encodé puis décodé
    #echo  "On fait un diff avec le fichier $NomFile.enc.dec pour vérifier"
    
    
    diff -u $NomFile $NomFile.enc.dec  > $NomFile.diff

    echo ""
done
