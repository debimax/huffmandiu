import os
import sys

class BasePlugin:
    @classmethod
    def name(cls):
        return cls.__module__.rpartition('.')[-1]

    @classmethod
    def find(cls, reqname):
        #print("find called from Parser for", reqname)
        if reqname == cls.name():
            return cls
        return None

class Parser(BasePlugin):
    def __init__(self, filename):
        self.filename = filename
        self.mode = None

    def tokens(self):
        raise NotImplementedError('Method tokens not implemented in parser '+self.name())

    def write_token(self, tok, bw):
        """Must write the token in the bitbybit writer

        read_token will be required to reread the symbol
        """
        raise NotImplementedError('Method write_token not implemented in parser '+self.name())

    def read_token(self, br):
        """Must read a token writen by write_token

        """
        raise NotImplementedError('Method read_token not implemented in parser '+self.name())

    def token2bytes(self, token):
        """Must convert the token to a bytearray
        """
        raise NotImplementedError('Method token2bytes not implemented in parser '+self.name())

    def print_token(self, token, ofile=None):
        """Write the token into the file whose filename was given when initializing
        """
        return ofile.write(self.token2bytes(token))

    @property
    def EOFtoken(self):
        """Must return a token meaning the EOF

        This token must be writable and readable with read/write_token
        but it has no other meaning
        """
        raise NotImplementedError('Method token2bytes not implemented in parser '+self.name())


class BitAccess(BasePlugin):
    @classmethod
    def pad_right(cls, bits):
        """
        pad with zeroes back to get a 8-length 0/1 string
        """
        return bits + '0' *(8 - len(bits))

    @classmethod
    def pad_left(cls, bits):
        """
        pad with zeroes front to get a 8-length 0/1 string
        """
        return '0' *(8 - len(bits)) + bits

    @classmethod
    def byte_to_bits(cls, byte):
        """
        convert an int to a 8-length 0/1 string
        """
        bits = bin(byte)[2:]
        assert len(bits) <= 8
        return cls.pad_left(bits)

    @classmethod
    def char_to_bits(cls, char):
        """
        convert a char to a 8-length 0/1 string
        """
        return cls.byte_to_bits(ord(char))

class BitReader(BitAccess):
    def __init__(self, infile):
        if isinstance(infile, str):
            # We have a filename
            self.to_close = True
            self.f_in = open(infile, 'rb')
        elif infile is None:
            # Nothing, using stdin
            self.to_close = None
            self.f_in = os.fdopen(sys.stdin.fileno(), 'rb')
        else:
            # An already opened file, reopening as binary
            self.to_close = True
            self.f_in = os.fdopen(infile.fileno(), 'rb')

    def _get_raw_bytes(self, nb=1):
        val = self.f_in.read(nb)
        if len(val) != nb:
            raise EOFError()
        return val

    def read(self, nbits):
        raise NotImplementedError('Method read not implemented in BitReader '+self.name())

    def read_bit(self):
        return self.read(1)

    def read_char(self):
        bits = self.read(8)
        return chr(int(bits, 2))

    def read_bytes(self, nb=1):
        b = bytearray()
        for _ in range(nb):
            bits = self.read(8)
            #print("bits: ", bits)
            b.extend(bytes([int(bits, 2)]))
        return b

    def flush(self):
        if self.to_close:
            self.f_in.close()
        self.f_in = None
        self.to_close = None

class BitWriter(BitAccess):
    def __init__(self, outfile):
        if isinstance(outfile, str):
            # We have a filename
            self.to_close = True
            self.outfile = open(outfile, 'wb')
        elif outfile is None:
            # Nothing, using stdout
            self.to_close = False
            sys.stdout.flush()
            self.outfile = os.fdopen(sys.stdout.fileno(), "wb")
        else:
            # An already opened file, reopening as binary
            outfile.flush()
            self.to_close = True
            self.outfile = os.fdopen(outfile.fileno(), "wb")

    def write(self, bits):
        raise NotImplementedError('Method read not implemented in BitWriter '+self.name())

    def write_char(self, char):
        bits = self.char_to_bits(char)
        self.write(bits)

    def write_bytes(self, bytes_to_write, nb=None):
        if nb is None:
            nb = len(bytes_to_write)
        for i in range(nb):
            b = bytes_to_write[i]
            bits = self.byte_to_bits(b)
            self.write(bits)

    def flush(self):
        if self.to_close:
            self.outfile.close()
