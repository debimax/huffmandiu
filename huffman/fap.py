
class FAP:

    def __init__(self):
        self.values = []

    @property
    def size(self):
        return len(self.values)

    def pop(self):
        assert self.size > 0
        return self.values.pop()

    def insert(self, element):
        """
        element must have a 'freq' field
        """

        size = self.size
        prio = element.freq
        place = 0
        while place < size and self.values[place].freq >= prio:
            place = place + 1
        self.values.insert(place, element)

    def __str__(self):
        ret = ''
        for node in reversed(self.values):
            ret += "fap: Freq: "+str(node.freq)+' Arbre:\n' \
            + node.__str__(2, "code: ")
        return ret
