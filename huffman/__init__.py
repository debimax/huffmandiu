
import os
import sys
from straight.plugin import load
from .libs import BitReader, BitWriter, Parser

if os.getenv('HUFFMAN_CORRECTION'):
    from .correction import analyse_frequence, arbre_huffman, table_correspondance, \
        ecrire_arbre, encode_huff, construit_huffman, decode_huff
else:
    from .travail import analyse_frequence, arbre_huffman, table_correspondance, \
        ecrire_arbre, encode_huff, construit_huffman, decode_huff

class Error(Exception):
    """Base class for exceptions in this module."""
    
class ArgError(Error):
    """Exception raised for errors in the arguments.

    Attributes:
        argument -- argument name which has a wrong value
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.argument = expression
        self.message = message
        super(ArgError, self).__init__()


_parsers_plugins = load('huffman.parsers', subclasses=Parser)
_bitreaders_plugins = load('huffman.bitbybit', subclasses=BitReader)
_bitwriters_plugins = load('huffman.bitbybit', subclasses=BitWriter)

class Factory: # pylint: disable=too-few-public-methods
    parsercls = None
    bitreadercls = None
    bitwritercls = None
    debug = None

def setup(args):
    try:
        Factory.parsercls = _parsers_plugins.first('find', args.parsername)
    except ValueError as e:
        raise ArgError('parsername', 'invalid parser name '+args.parsername) from e

    try:
        Factory.bitreadercls = _bitreaders_plugins.first('find', args.io)
    except ValueError as e:
        raise ArgError('io', 'invalid BitReader name '+args.io) from e

    try:
        Factory.bitwritercls = _bitwriters_plugins.first('find', args.io)
    except ValueError as e:
        raise ArgError('io', 'invalid BitWriter name '+args.io) from e

    if args.debug:
        Factory.debug = True

def parsers_list():
    return list(_parsers_plugins.call("name"))

def bitbybit_list():
    """List of acceptable io-bit-by-bit modules
    """
    readers = list(_bitreaders_plugins.call("name"))
    writers = list(_bitwriters_plugins.call("name"))
    # both reader and writer must be available
    return [name for name in readers if name in writers]

def encode(ifilename, ofilename=None):
    parser = Factory.parsercls(ifilename)
    bw = Factory.bitwritercls(ofilename)

    freq = analyse_frequence(parser)
    if Factory.debug:
        print(freq)

    huff = arbre_huffman(freq)
    if Factory.debug:
        print(huff)

    table = table_correspondance(huff)
    if Factory.debug:
        print(table)

    ecrire_arbre(huff, bw, parser)
    encode_huff(parser, table, bw)

    bw.flush()

def decode(ifilename=None, ofilename=None):
    parser = Factory.parsercls(None)
    br = Factory.bitreadercls(ifilename)

    huff = construit_huffman(parser, br)
    if Factory.debug:
        print(huff)

    if isinstance(ofilename, str):
        # We have a filename
        with open(ofilename, 'wb') as output:
            decode_huff(huff, br, parser, output)
    elif ofilename is None:
        # Nothing, using stdout
        sys.stdout.flush()
        with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as output:
            decode_huff(huff, br, parser, output)
    else:
        # An already opened file, reopening as binary
        ofilename.flush()
        with os.fdopen(ofilename.fileno(), "wb") as output:
            decode_huff(huff, br, parser, output)
