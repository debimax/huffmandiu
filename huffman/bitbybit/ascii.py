
import huffman

class BitReader(huffman.BitReader):
    def read(self, nbits):
        val = self._get_raw_bytes(nbits)
        ret = val.decode('utf-8')
        return ret

class BitWriter(huffman.BitWriter):
    def write(self, bits):
        self.outfile.write(bits.encode('ascii', 'strict'))
        self.outfile.flush()
