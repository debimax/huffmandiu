
import huffman

class BitReader(huffman.BitReader):
    def __init__(self, infile):
        super(BitReader, self).__init__(infile)
        self.buffer = ''

    def read_bit(self):
        return self.read(1)

    def read(self, nbits):
        while len(self.buffer) < nbits:
            char = self._get_raw_bytes(1)
            bits = self.char_to_bits(char)
            self.buffer += bits

        bits = self.buffer[:nbits]
        self.buffer = self.buffer[nbits:]
        return bits

class BitWriter(huffman.BitWriter):
    def __init__(self, outfile):
        super(BitWriter, self).__init__(outfile)
        self.buffer = ''
        self.ba = bytearray()

    def write(self, bits):
        self.buffer += bits
        while len(self.buffer) >= 8:
            char = self.buffer[:8]
            self.buffer = self.buffer[8:]
            self.ba.append(int(char, 2))

    def flush(self):
        if self.buffer != '':
            # pad with only 1(as the eof is made of 1's
            self.buffer += '1' *(8 - len(self.buffer))
            self.ba.append(int(self.buffer, 2))
            self.buffer = ''

        self.outfile.write(self.ba)
        self.outfile.flush()
        super(BitWriter, self).flush()
