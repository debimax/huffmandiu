"""Basic tree representation for Huffman algorithm needs"""

class Node:
    """Node object to build trees"""
    def __init__(self, symbole, freq, gauche=None, droit=None):
        self.symbole = symbole
        self.freq = freq
        self.gauche = gauche
        self.droit = droit


    def is_leaf(self):
        """Return true if the node is a leaf"""
        return not self.gauche and not self.droit

    def __str__(self, indent=0, cur=""):
        ret = ""
        if self.symbole:
            ret += "  "*indent + repr(self.symbole) + " -> " + str(cur) + "\n"

        if self.gauche:
            ret += "  "*indent + "0:\n"
            ret += self.gauche.__str__(indent+1, cur + "0")

        if self.droit:
            ret += "  "*indent + "1:\n"
            ret += self.droit.__str__(indent+1, cur + "1")

        return ret

    def debug(self, indent=0, cur=""):
        """Print a ascii drawing of the tree"""
        print(self.__str__(indent, cur))
