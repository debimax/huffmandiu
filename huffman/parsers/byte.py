
import huffman

class Parser(huffman.Parser):
    def tokens(self):
        with open(self.filename, 'rb') as f:
            while True:
                byte_s = f.read(1)
                if not byte_s:
                    break
                byte = byte_s[0]
                yield byte

    def write_token(self, tok, bw):
        bw.write_char(bytes([tok]))

    def read_token(self, br):
        return ord(br.read_bytes(1))

    def token2bytes(self, token):
        return bytes([token])

    @property
    def EOFtoken(self):
        return ord('E')
