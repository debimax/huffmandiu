import io
import huffman

class Parser(huffman.Parser):
    def tokens(self):
        with io.open(self.filename, mode="r", encoding="utf-8") as f:
            while True:
                byte_s = f.read(1)
                if not byte_s:
                    break
                byte = byte_s[0]
                yield byte

    def write_token(self, tok, bw):
        bw.write_bytes(tok.encode('utf-8'))

    def read_token(self, br):
        b = br.read_bytes(1)
        #print("B: ", type(b), b)
        while True:
            try:
                tok = b.decode('utf-8', 'strict')
                return tok
            except UnicodeDecodeError:
                b.append(ord(br.read_bytes(1)))
                #print("B+: ", type(b), b)
                
    def token2bytes(self, token):
        return token.encode("utf-8")

    @property
    def EOFtoken(self):
        return 'E'
