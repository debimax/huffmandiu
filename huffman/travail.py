#Hautcoeur Stéphane
#Meilland Jean_Claude
#Melioli Edith
#Rajaomanana Ingrid

from .fap import FAP
from .arbre import Node
from math  import log

# Les fonctions de ce fichier sont utilisées par le squelette
# du programme. Elles doivent être complétées.

def analyse_frequence(parser):
    """Calcul le nombre d'occurrence de chaque symbole/token

    Renvoie un dictionnaire dont les clés sont les symboles
    du texte et les valeurs le nombre d'occurrence du sybole
    """
    freq = {}
    taille=0
    for tok in parser.tokens():
        taille = taille+1
        if tok in  freq:
            freq[tok]=freq[tok]+1
        else:
            freq[tok]=1
    entropie=0
    for valeur in  freq.values():
        p=valeur/taille
        entropie-=p*log(p,2)
    print(f"entropie: {entropie}")
    # Ajout  du caractère réservé
    freq[0]=0
    return freq

def arbre_huffman(freq):
    """ Construit un arbre de huffman à partir du dictionnaire de fréquences
    """
    arbre = None
    freq_min=FAP()
    for key in freq: #on passe en format objet node/feuille puis on insère dans la file à priorité décroissante
        arbre=Node(key, freq[key])
        freq_min.insert(arbre)

    while freq_min.size>1: #tant qu'il reste des noeuds dans la FAP
        min1=freq_min.pop() #récupérer les 2 mins
        min2=freq_min.pop()
        arbre=Node(None,min1.freq+min2.freq,min1,min2) #affecter les noeuds fils et la somme des fréquence pour la fréquence - somme des symboles pour se repérer
        freq_min.insert(arbre) #remettre l'arbre dans la FAP au bon endroit selon sa frequence
    return arbre

def table_correspondance(huff):
    """ Construit le dictionnaire des codes
    Renvoie un dictionnaire dont les clés sont les symboles de l'arbre de huffman
    et les valeurs sont des chaînes de caractères '0' et '1'
    qui représentent le code du symbole
    """
    table={}
    def calcul_code(code,ss_arbr):
        # Calcul  du chemin vers la feuille
        if ss_arbr.gauche != None:
            calcul_code(code+"0",ss_arbr.gauche)
            calcul_code(code+"1",ss_arbr.droit)
        else:
        #On arrive à une feuille
            table[ss_arbr.symbole]=code
    calcul_code("", huff)

    return table

def ecrire_arbre(huff, bw, parser):
    """ Écrit l'arbre de huffman dans le fichier de sortie
   
    huff est l'arbre de huffman
    bw est un objet de type 'BitWrite' permettant d'écire bit-à-bit
    parser est un objet dérivant de huffman.Parser que l'on doit utiliser
    pour écrire les symboles (méthode write_token)
    """
    # À compléter
    # Vous aurez à utiliser des choses comme
    # ... bw.write('0')
    # ... bw.write('1')
    # ... parser.write_token(node.symbole, bw)
    # Encodage de l'arbre avec 1 qui indique l'arrivée d'une feuille
    if not huff.is_leaf():
        bw.write('0')
        ecrire_arbre(huff.gauche, bw, parser)
        ecrire_arbre(huff.droit, bw,parser)
    else:
        bw.write('1')
        parser.write_token(huff.symbole, bw)

def encode_huff(parser, table, bw):
    """Écrit l'encodage des tokens dans le fichier de sortie
   
    table est la table de correspondance permettant d'avoir le code des tokens
    """
    for c in parser.tokens():
        bw.write(str(table[c]))
    # Écriture du caractère réservé
    bw.write(str(table[0]))

def construit_huffman(parser, br):
    """Construit l'arbre de huffman à partir du fichier encodé
    br est un objet BitRead permettant de lire le fichier bit-à-bit
    parser est un objet dérivant de huffman.Parser que l'on doit utiliser
    pour lire les symboles (méthode read_token)
    """
    b=br.read(1)
    if b=="1":
        return Node(parser.read_token(br),0)
    else:
        gauche=construit_huffman(parser,br)
        droite=construit_huffman(parser,br)
        return Node(None,None,gauche,droite)


def decode_huff (huff, br, parser, output):
    """Écrit le fichier de sortie décompressé

    huff est l'arbre de huffman
    br est un objet BitRead permettant d'accéder au contenu compressé
    parser est un objet dérivant de huffman.Parser que l'on doit utiliser
    pour écrire les symboles (méthode print_token)
    """

    # À compléter
    # Vous aurez à utiliser des choses comme
    # ... bit = br.read()
    # ... symbole = ...
    # ... parser.print_token(symbole)
    
    while 1 :
        arbre=huff
        while not arbre.is_leaf():
            bit=br.read(1)
            if bit == '0' :
                arbre=arbre.gauche
            else:
                arbre=arbre.droit
        #Utilisation du caractère réservé pour sortir de la boucle
        if arbre.symbole==0:
            return
        else :
            parser.print_token(arbre.symbole, output)
