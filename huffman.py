#!/usr/bin/env python3

import huffman
import sys
from huffman.parsers.bitbybit import BitWriter, BitReader
#from huffman.bitbybit import BitWriter, BitReader

# lecture fichier
# token/char
# table des symboles
# fréquences
# arbre
# - contruction (FAP)
# - écriture
# table de correspondance
# encodage
# - utilisation table
# - écriture bit-a-bit
# lecture arbre
# lecture bit-a-bit
# écriture fichier
# tests
# démo


def decode (input_file, output_file=None):

    br = BitReader (input_file)

    huff = construit_huffman (br)
    huff.debug()

    texte = decode_huff (huff, br)


    if output_file:
        with open(output_file, 'w') as out:
            out.write (texte)
    else:
        print (texte)

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Huffman compressor/decompressor')
    parser.set_defaults(encode=True)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-c', '--compress', dest='encode', action='store_true',
                        help='compress file with huffman algorithm')
    group.add_argument('-d', '--decompress', dest='encode', action='store_false',
                        help='decompress file with huffman algorithm')
    parser.add_argument('-o', '--output', metavar='file', type=str,
                        dest='destfilename', action='store',
                        help='output file name (defaut: output to stdout)')
    parser.add_argument('-p', '--parser', metavar='name', type=str,
                        dest='parsername', action='store', default='byte',
                        help='parser to be used to get tokens (default: byte)')
    parser.add_argument('files', nargs='*',
                        help='files to manage')
    args = parser.parse_args()
    if len(args.files) > 1 and args.destfilename is not None:
        parser.print_help()
        print("")
        print("Error: You cannot specify destination filename (-o) with multiple input files")
        sys.exit(1)
    
    if len(args.files) == 0:
        print("Need file(s) to compress/decompress")
        exit(1)

    print(args)
    if args.encode:
        for file in args.files:
            huffman.encode(ifilename=file, ofilename=args.destfilename,
                           parsername=args.parsername)
    else:
        for file in args.files:
            huffman.decode(ifilename=file, ofilename=args.destfilename,
                           parsername=args.parsername)  

main()
