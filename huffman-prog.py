#!/usr/bin/env python3

import sys
import argparse
import os
sys.path.append(os.path.dirname(__file__))
import huffman

def main():
    parser = argparse.ArgumentParser(description='Huffman compressor/decompressor')
    parser.set_defaults(encode=True, io='ascii', ascii=True)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-c', '--compress', dest='encode', action='store_true',
                       help='compress file with huffman algorithm')
    group.add_argument('-d', '--decompress', dest='encode', action='store_false',
                       help='decompress file with huffman algorithm')
    parser.add_argument('-o', '--output', metavar='file', type=str,
                        dest='destfilename', action='store',
                        help='output file name (defaut: output to stdout)')
    parser.add_argument('--debug',
                        dest='debug', action='store_true', default=False,
                        help='print intermediate objects')
    parser.add_argument('-p', '--parser', metavar='name', type=str,
                        dest='parsername', action='store', default='byte',
                        choices=huffman.parsers_list(),
                        help='parser to be used to get tokens (default: byte)')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-a', '--ascii',
                       dest='io', action='store_const', const='ascii',
                       help='use ascii io module to read/write compressed files')
    group.add_argument('-b', '--binary',
                       dest='io', action='store_const', const='binary',
                       help='use binary io module to read/write compressed files')
    group.add_argument('--io', metavar='name', type=str,
                       dest='io', action='store',
                       choices=huffman.bitbybit_list(),
                       help='io module to use to read/write compressed files')
    parser.add_argument('files', nargs='*',
                        help='files to manage')
    args = parser.parse_args()
    if len(args.files) > 1 and args.destfilename is not None:
        parser.print_help()
        print("")
        print("Error: You cannot specify destination filename (-o) with multiple input files")
        sys.exit(1)

    if len(args.files) == 0:
        print("Need file(s) to compress/decompress")
        sys.exit(1)

    # print(args)
    huffman.setup(args)

    if args.encode:
        for file in args.files:
            if args.destfilename is None:
                ofilename = file + '.enc'
            else:
                ofilename = args.destfilename
            huffman.encode(ifilename=file, ofilename=ofilename)
    else:
        for file in args.files:
            if args.destfilename is None:
                ofilename = file + '.dec'
            else:
                ofilename = args.destfilename
            huffman.decode(ifilename=file, ofilename=ofilename)

main()
